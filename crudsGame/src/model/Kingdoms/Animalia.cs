﻿using crudsGame.src.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crudsGame.src.model.Kingdoms
{
    internal class Animalia:IKingdom
    {
        public override string ToString()
        {
            return "Animalia";
        }
    }
}
